// 1. თითოეული სტუდენტისთვის შექმნათ შესაბამისი მასივი და მოცემული ინფორმაცია ჩაწეროთ შიგნით
const students = [];

students[0] = {
    firstName: 'Jan',
    lastName: 'Reno',
    age: '26',
    scores: {
        javascript: 62,
        react: 57,
        python: 88,
        java: 90
    },
    gpas: {
        jsGpa: 1,
        reactGpa: 0.5,
        pythonGpa: 3,
        javaGpa: 3
    },
    FrontAvarage: {
        
    }
}
students[1] = {
    firstName: 'Klod',
    lastName: 'Mone',
    age: '19',
    scores: {
        javascript: 77,
        react: 52,
        python: 92,
        java: 67
    },
    gpas: {
        jsGpa: 2,
        reactGpa: 0.5,
        pythonGpa: 4,
        javaGpa: 1
    }
}
students[2] = {
    firstName: 'Van',
    lastName: 'Gogi',
    age: '21',
    scores: {
        javascript: 51,
        react: 98,
        python: 65,
        java: 70
    },
    gpas: {
        jsGpa: 0.5,
        reactGpa: 4,
        pythonGpa: 1,
        javaGpa: 1
    }
}
students[3] = {
    firstName: 'Dam',
    lastName: 'Squeari',
    age: '36',
    scores: {
        javascript: 82,
        react: 53,
        python: 80,
        java: 65
    },
    gpas: {
        jsGpa: 3,
        reactGpa: 0.5,
        pythonGpa: 2,
        javaGpa: 1
    }
}

// 2. თითოეული სტუდენტისთვის უნდა შეაგროვოთ (გამოთვალოთ) შემდეგი ინფორმაცია: ქულების ჯამი, ქულების საშუალო, GPA. ზემოთხსენებული მასივი ამ ინფორმაციასაც უნდა მოიცავდეს.
const credits = {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3
}
// jan
const creditSum = credits.javascript + credits.react + credits.python + credits.java;

// add to obj
students[0].accomplishment = {};
students[0].accomplishment.sum = students[0].scores.javascript + students[0].scores.react + students[0].scores.python + students[0].scores.java;
students[0].accomplishment.avarage = students[0].accomplishment.sum  / 4;
students[0].accomplishment.gpa = ((students[0].gpas.jsGpa * credits.javascript) + (students[0].gpas.reactGpa * credits.react) + (students[0].gpas.pythonGpa * credits.python) + (students[0].gpas.javaGpa * credits.java)) / creditSum;
console.log(students[0]);

// klod
// add to obj
students[1].accomplishment = {};
students[1].accomplishment.sum = students[1].scores.javascript + students[1].scores.react + students[1].scores.python + students[1].scores.java;;
students[1].accomplishment.avarage = students[1].accomplishment.sum / 4;
students[1].accomplishment.gpa = ((students[1].gpas.jsGpa * credits.javascript) + (students[1].gpas.reactGpa * credits.react) + (students[1].gpas.pythonGpa * credits.python) + (students[1].gpas.javaGpa * credits.java)) / creditSum;
console.log(students[1]);

// van
// add to obj
students[2].accomplishment = {};
students[2].accomplishment.sum = students[2].scores.javascript + students[2].scores.react + students[2].scores.python + students[2].scores.java;
students[2].accomplishment.avarage = students[2].accomplishment.sum / 4;
students[2].accomplishment.gpa =  ((students[2].gpas.jsGpa * credits.javascript) + (students[2].gpas.reactGpa * credits.react) + (students[2].gpas.pythonGpa * credits.python) + (students[2].gpas.javaGpa * credits.java)) / creditSum;
console.log(students[2]);

 // dam
// add to obj
students[3].accomplishment = {};
students[3].accomplishment.sum = students[3].scores.javascript + students[3].scores.react + students[3].scores.python + students[3].scores.java;
students[3].accomplishment.avarage = students[3].accomplishment.sum  / 4;
students[3].accomplishment.gpa = ((students[3].gpas.jsGpa * credits.javascript) + (students[3].gpas.reactGpa * credits.react) + (students[3].gpas.pythonGpa * credits.python) + (students[3].gpas.javaGpa * credits.java)) / creditSum;
console.log(students[3]);

// 3.გამოთვალეთ 4 - ვე სტუდენტის საშუალო არითმეტიკული (ანუ ვაფშე ყველა ქულა და ყველა საგანი), სტუდენტებს, რომელთა ცალკე საშუალო არითმეტიკული მეტი აქვთ ვიდრე საერთო არითმეტიკული, მივანიჭოთ "წითელი დიპლომის მქონეს" სტატუსი, ხოლო ვისაც საერთო საშუალოზე ნაკლები აქვს - "ვრაგ ნაროდა" სტატუსი

const avarageAll = (students[0].accomplishment.avarage + students[1].accomplishment.avarage + students[2].accomplishment.avarage + students[3].accomplishment.avarage) / 4;

students[0].accomplishment.avarage > avarageAll ? console.log("jan you earned წითელი დიპლომი") : console.log("jan is vrag naroda")
students[1].accomplishment.avarage > avarageAll ? console.log(`klod you earned წითელი დიპლომი`) : console.log("klod is vrag naroda")
students[2].accomplishment.avarage > avarageAll ? console.log(`van you earned წითელი დიპლომი`) : console.log("van is vrag naroda")
students[3].accomplishment.avarage > avarageAll ? console.log(`dam you earned წითელი დიპლომი`) : console.log("dam is vrag naroda")

// 4 უნდა გამოავლინოთ საუკეთესო სტუდენტი GPA ს მიხედვით

if (students[0].accomplishment.gpa > students[1].accomplishment.gpa && students[0].accomplishment.gpa > students[2].accomplishment.gpa && students[0].accomplishment.gpa > students[3].accomplishment.gpa) {
    console.log("Jan has max  gpa points")
} else if (students[1].accomplishment.gpa > students[0].accomplishment.gpa && students[1].accomplishment.gpa > students[2].accomplishment.gpa && students[1].accomplishment.gpa > students[3].accomplishment.gpa) {
    console.log("Klod has max  gpa points")
} else if (students[2].accomplishment.gpa > students[1].accomplishment.gpa && students[2].accomplishment.gpa > students[0].accomplishment.gpa && students[2].accomplishment.gpa > students[3].accomplishment.gpa) {
    console.log("Van has max gpa points")
} else if (students[3].accomplishment.gpa > students[1].accomplishment.gpa && students[3].accomplishment.gpa > students[2].accomplishment.gpa && students[3].accomplishment.gpa > students[0].accomplishment.gpa) {
    console.log("Dam has max gpa points")
} else {
    console.log("equal")
}

//  5.უნდა გამოავლინოთ საუკეთესო სტუდენტი 21 + ასაკში საშუალო ქულების მიხედვით (აქ 21+ იანის ხელით ამორჩევა არ იგულისხმება, უნდა შეამოწმოთ როგორც საშუალო ქულა, ასევე ასაკი)

if (students[0].age > 21 && (students[0].accomplishment.avarage > students[1].accomplishment.avarage && students[0].accomplishment.avarage > students[2].accomplishment.avarage && students[0].accomplishment.avarage > students[3].accomplishment.avarage)) {
    console.log("Jan has max  points")
} else if (students[1].age > 21 && (students[1].accomplishment.avarage > students[0].accomplishment.avarage && students[1].accomplishment.avarage > students[1].accomplishment.avarage && students[1].accomplishment.avarage > students[3].accomplishment.avarage)) {
    console.log("Klod has max  points")
} else if (students[2].age && (students[2].accomplishment.avarage > students[0].accomplishment.avarage && students[2].accomplishment.avarage > students[1].accomplishment.avarage && students[2].accomplishment.avarage > students[3].accomplishment.avarage)) {
    console.log("Van has max  points")
} else if (students[3].age && (students[3].accomplishment.avarage > students[0].accomplishment.avarage && students[3].accomplishment.avarage > students[1].accomplishment.avarage && students[3].accomplishment.avarage > students[2].accomplishment.avarage)) {
    console.log("Dam has max  points")
} else {
    console.log("equal")
}

// 6. უნდა გამოავლინოთ სტუდენტი რომელიც საუკეთესოა ფრონტ-ენდის საგნებში საშუალო ქულების მიხედვით 

students[0].accomplishment.frontAvarage = (students[0].scores.javascript + students[0].scores.react) / 2
students[1].accomplishment.frontAvarage  = (students[1].scores.javascript + students[1].scores.react) / 2
students[2].accomplishment.frontAvarage  = (students[2].scores.javascript + students[2].scores.react) / 2
students[3].accomplishment.frontAvarage  = (students[3].scores.javascript + students[3].scores.react) / 2

if (students[0].accomplishment.frontAvarage > students[1].accomplishment.frontAvarage && students[0].accomplishment.frontAvarage > students[2].accomplishment.frontAvarage && students[0].accomplishment.frontAvarage > students[3].accomplishment.frontAvarage) {
    console.log(`Jan has max ${students[0].accomplishment.frontAvarage} front points`)
} else if (students[1].accomplishment.frontAvarage > students[0].accomplishment.frontAvarage && students[1].accomplishment.frontAvarage > students[2].accomplishment.frontAvarage && students[1].accomplishment.frontAvarage > students[2].accomplishment.frontAvarage) {
    console.log(`Klod has max ${students[1].accomplishment.frontAvarage} front points`)
} else if (students[2].accomplishment.frontAvarage > students[0].accomplishment.frontAvarage && students[2].accomplishment.frontAvarage > students[1].accomplishment.frontAvarage && students[2].accomplishment.frontAvarage > students[3].accomplishment.frontAvarage) {
    console.log(`Van has max ${students[2].accomplishment.frontAvarage} front points`)
} else if (students[3].accomplishment.frontAvarage > students[0].accomplishment.frontAvarage && students[3].accomplishment.frontAvarage > students[1].accomplishment.frontAvarage && students[3].accomplishment.frontAvarage > students[2].accomplishment.frontAvarage) {
    console.log(`Dam has max ${students[3].accomplishment.frontAvarage} front points`)
} else {
    console.log("equal")
}